import socket
import socketserver
import threading
from tools.logger import logger
from tools.config import CONF
import queue

import kinesis_motor

class TCPServer(socketserver.TCPServer):

    motor_threads: dict[str, kinesis_motor.KinesisMotor] = dict()
    
    def connect_motor(self, device_addr: str = '/dev/ttyUSB0') -> bool:
        try:
            motor_thread = kinesis_motor.KinesisMotor(device_addr)
            self.motor_threads[device_addr] = motor_thread
            motor_thread.start()
            return True
        except:
            logger.error(f"Could not open device at {device_addr}")
            return False

    def serve_forever(self):
        try:
            super().serve_forever()
        except KeyboardInterrupt:
            pass
        logger.info("TCP server stopped.")
        return
    
    def stop_motor_threads(self) -> None:
        for motor_thread in self.motor_threads.values():
            motor_thread.stop.set()

    def shutdown(self):
        self.stop_motor_threads()
        super().shutdown()

class TCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    server : TCPServer

    def handle(self):
        # self.request is the TCP socket connected to the client
        data = self.request.recv(1024).strip().decode('utf-8')

        # send the data to the motor
        for motor_thread in self.server.motor_threads.values():
            motor_thread.command_queue.put(data)

def main():

    host: str = CONF['general']['default_tcp_host']
    port: int = int(CONF['general']['default_tcp_port'])
    device_addr: str = CONF['general']['default_device']

    # Create the server, binding to localhost on port 9999
    server = TCPServer((host, port), TCPHandler)
    server.connect_motor(device_addr=device_addr)
    server.serve_forever()


