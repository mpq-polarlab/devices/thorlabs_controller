from PyQt5 import QtWidgets, QtWidgets, QtCore, QtGui
from tools.logger import logger

class StatusLED(QtWidgets.QWidget):
    '''Generated with ChatGPT, later modified. But wtf...'''

    status_dict : 'dict[int, tuple[str, QtGui.QColor]]' = {0 : ('Idle', QtGui.QColor(0, 255, 0))}

    def __init__(self, parent=None, prefix = 'Status: '):
        super().__init__(parent)

        self.prefix = prefix
        self.status = 0
        
        self.label = QtWidgets.QLabel("")
        self.label.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label.setMargin(0)

        # create layout with space for LED
        layout = QtWidgets.QHBoxLayout(self)
        layout.addWidget(self.label)
        spacer = QtWidgets.QSpacerItem(20, 20)
        layout.addItem(spacer)
        layout.setContentsMargins(0,0,0,0)

        # set status
        self.set_status(self.status)

    def set_status(self, status : int) -> None:

        if not status in self.status_dict.keys():
            logger.error("Unknown status %i" % status)
            return
        else:
            self.status = status

        (label, _) = self.status_dict[self.status]
        self.update()
        self.label.setText(self.prefix + label)  

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)

        (_, color) = self.status_dict[self.status]
        painter.setBrush(QtGui.QBrush(color))
        w, h = self.width(), self.height()
        pad = 3
        painter.drawEllipse(w-h + pad, pad, h-2*pad, h - 2*pad)