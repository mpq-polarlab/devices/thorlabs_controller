import PyQt5.QtWidgets as QtWidgets
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui

from tools.logger import logger
from widgets.status_led import StatusLED

class DeviceManager(QtWidgets.QWidget):
    """
    Widget representing the area where the thumbnails are displayed.
    """

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent 
        self.devices: 'dict[str, DeviceWidget]' = dict()

        # appearance and layout
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, 
                                            QtWidgets.QSizePolicy.Preferred))
        self.layout = QtWidgets.QGridLayout(self)

    def add_device(self, addr, model: str = '', serial: str = '') -> None:
        """
        Creates new thumbnail and places it in in thumbnail area.
        """

        # check if device already exists
        if addr in self.devices: return
        
        # create new device widget
        device_widget = DeviceWidget(parent=self, addr=addr, model=model, serial=serial)
        self.devices[addr] = device_widget
        self.layout.addWidget(device_widget, len(self.devices)-1, 0)


    def clear(self):

        for widget in self.devices.values():
            widget.deleteLater()

        self.devices.clear()

class DeviceWidget(QtWidgets.QFrame):

    # widgets
    position_indicator: QtWidgets.QLineEdit
    status_moving: StatusLED
    status_homed: StatusLED


    def __init__(self, addr, model: str = '', serial: str = '', parent=None):

        super().__init__(parent=parent)
        
        self.addr = addr
        self.model = model
        self.serial = serial
        self.setup_ui()


    def setup_ui(self) -> None:

        # geometry and appearance
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)

        self.position_indicator = QtWidgets.QLineEdit(self)
        self.position_indicator.setReadOnly(True)
        self.position_indicator.setPlaceholderText('Position')

        self.status_moving = StatusLED(self, 'Moving ')
        self.status_moving.status_dict = {0: ('', QtCore.Qt.black), 1: ('', QtCore.Qt.green)}
        self.status_homed = StatusLED(self, 'Homed ')
        self.status_homed.status_dict = {0: ('', QtCore.Qt.black), 1: ('', QtCore.Qt.green), -1: ('', QtCore.Qt.red)}
        self.status_homed.set_status(0)
        self.status_moving.set_status(0)

        # create labels
        addr_label = QtWidgets.QLineEdit(self.addr)
        addr_label.setPlaceholderText('Address'); addr_label.setReadOnly(True)
        model_label = QtWidgets.QLineEdit(self.model)
        model_label.setPlaceholderText('Model'); model_label.setReadOnly(True)
        serial_label = QtWidgets.QLineEdit(self.serial)
        serial_label.setPlaceholderText('Serial'); serial_label.setReadOnly(True)
        
        # add buttons and edits
        hlayout1 = QtWidgets.QHBoxLayout()
        hlayout1.addWidget(QtWidgets.QLabel('Device address:'))
        hlayout1.addWidget(addr_label)
        hlayout1.addWidget(QtWidgets.QLabel('Model:'))
        hlayout1.addWidget(model_label)
        hlayout1.addWidget(QtWidgets.QLabel('Serial:'))
        hlayout1.addWidget(serial_label)

        # add status information
        hlayout2 = QtWidgets.QHBoxLayout()
        hlayout2.addWidget(QtWidgets.QLabel('Position:'))
        hlayout2.addWidget(self.position_indicator)
        hlayout2.addWidget(self.status_moving)
        hlayout2.addWidget(self.status_homed)

        # apply layout
        vlayout = QtWidgets.QVBoxLayout()
        vlayout.addLayout(hlayout1)
        vlayout.addLayout(hlayout2)
        self.setLayout(vlayout)
    
    def update_status(self, new_status: dict) -> None:
        self.position_indicator.setText(str(new_status['position']) + ' ' + new_status['scale_unit'])
        self.status_moving.set_status(int(new_status['moving']))
        self.status_homed.set_status({(True, False): 1, (False, True): 0}.get((new_status['homed'], new_status['homing']), -1))
