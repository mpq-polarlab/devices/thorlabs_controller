import pylablib
from pylablib.devices import Thorlabs
import threading
import queue
import sys, os
import time
from typing import Any

from tools.logger import logger

class KinesisMotor(threading.Thread):
    '''Wrapper for KinesisMotor motor controller'''
    
    classid: str = None
    motor : Thorlabs.KinesisMotor
    device_info : Thorlabs.kinesis.TDeviceInfo
    command_queue : queue.Queue
    stop : threading.Event
    device_status: dict = dict()


    def __init__(self, addr) -> None:

        super().__init__()

        self.command_queue = queue.Queue()
        self.stop = threading.Event()

        try:
            self.motor = Thorlabs.KinesisMotor(addr, scale='stage')
            self.scale_unit = self.motor.get_scale_units()
            self.update_device_status()
            logger.info(f"Connected to motor on addr {addr}")
        except Exception as e:
            logger.error(f"Error while connecting to motor: {e}")
            raise e
        

        # get device info
        self.addr = addr
        self.device_info = self.motor.get_device_info()

    def home(self) -> None:
        '''Home the motor, only if not already homed'''
        
        try:
            if self.motor.is_homed():
                logger.info("Motor is already homed")
            else:
                self.motor.home(force=False, sync=False)
        except Thorlabs.base.ThorlabsTimeoutError:
            logger.error(f"Timeout error while homing")
        
        return
    
    def move_to(self, position: float, scale: bool = True) -> None:
        '''Move to position. If scale is True, position in pre-defined scale, otherwise in steps.'''
        try:
            self.motor.move_to(position, scale=scale)
        except Thorlabs.base.ThorlabsTimeoutError:
            logger.error(f"Timeout error while moving")
        return
    
    def get_position(self, scale: bool=True) -> float:
        '''Get position. If scale is True, position in pre-defined scale, otherwise in steps.'''
        return self.motor.get_position(scale=scale)
    
    
    def update_device_status(self) -> None:
        device_status = dict()
        
        # get status
        status = self.motor.get_status()
        device_status['connected'] = 'connected' in status
        device_status['moving'] = self.motor.is_moving()
        device_status['homed'] = 'homed' in status
        device_status['homing'] = 'homing' in status

        # get position
        device_status['position'] = self.get_position(scale=True)
        device_status['scale_unit'] = self.scale_unit

        self.device_status.update(device_status)

    
    def run(self) -> None:
        while self.stop.is_set() == False:

            self.update_device_status()
            
            if self.motor.is_moving(): continue

            if self.command_queue.empty(): 
                continue
            command = self.command_queue.get()
            logger.info(f"Received command: {command}")
            if command == 'HOME':
                self.home()
            elif command.startswith('MOVE_TO'):
                angle = float(command.split(' ')[1])
                self.move_to(angle)
            else:
                logger.error(f"Unknown command: {command}")

        self.motor.close()
        logger.info(f"Shut down motor thread for device {self.addr}")
        return
    

if __name__ == "__main__":
    logger.setLevel('INFO')

    motor = KinesisMotor('/dev/ttyUSB0')
    motor.start()
    motor.command_queue.put('MOVE_TO 0.015')
    motor.command_queue.put('MOVE_TO 0.001')
    motor.command_queue.put('HOME')
    #motor.command_queue.put('MOVE_TO 0')
    
    try:
        while True:
            logger.info(f"Position: {motor.device_status['position']} {motor.device_status['scale_unit']}") 
            time.sleep(0.25)
    except KeyboardInterrupt:
        motor.stop.set()
    
    
