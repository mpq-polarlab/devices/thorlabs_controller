import socket

def send_command_to_tcp_server(host, port, command):
    try:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect to the server
        sock.connect((host, port))

        # Send the command
        sock.sendall(command.encode('utf-8'))

        # Receive response from the server
        response = sock.recv(1024)  # Adjust buffer size as needed
        print(f"Server response: {response.decode('utf-8')}")

    except socket.error as e:
        print(f"Socket error: {e}")

    finally:
        # Close the connection
        sock.close()

# Example usage
host = '127.0.0.1'  # Server IP
port = 12346        # Server port

for command in ['HOME', 'MOVE_TO 25', 'Wrong command']:
    send_command_to_tcp_server(host, port, command)

