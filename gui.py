import sys, os
import threading

import tcp_server
from tools.logger import logger
from tools.config import CONF
from widgets.device_widget import DeviceManager

import PyQt5.QtWidgets as QtWidgets
import PyQt5.QtCore as QtCore

class ThorlabsMonitor(QtWidgets.QWidget):

    server: tcp_server.TCPServer

    def __init__(self):
        super().__init__()
        self.initUI()

        # start polling status
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.poll_status)
        self.timer.start(100) # 100 ms

    def closeEvent(self, event):
        '''Override close event to stop server'''
        self.server.shutdown()
        event.accept()
        
    def initUI(self):
        # Set the window title
        self.setWindowTitle('Thorlabs Monitor')
        
        # boxes
        self.setup_connection_box()
        self.setup_device_manager()

        
        # Create a layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tcp_server_group)
        layout.addWidget(self.device_manager_box)
        
        # Set the layout on the application's window
        self.setLayout(layout)

        # connect signals
        self.inp_connect.clicked.connect(self.connect_tcp_server)
        self.add_device_button.clicked.connect(self.add_device)

        # dummy device
        self.connect_tcp_server()
        self.add_device()

    def setup_connection_box(self) -> None:
        host, port = CONF['general']['default_tcp_host'], CONF['general']['default_tcp_port']

        self.inp_host: QtWidgets.QLineEdit = QtWidgets.QLineEdit(self, text=host)
        self.inp_port: QtWidgets.QLineEdit = QtWidgets.QLineEdit(self, text=str(port))
        self.inp_connect: QtWidgets.QPushButton = QtWidgets.QPushButton('Connect TCP', self)
        
        # create layout
        layout = QtWidgets.QGridLayout()

        layout.addWidget(QtWidgets.QLabel('Host:'), 0, 0)
        layout.addWidget(self.inp_host, 0, 1)
        layout.addWidget(QtWidgets.QLabel('Port:'), 0, 2)
        layout.addWidget(self.inp_port, 0, 3)
        layout.addWidget(self.inp_connect, 0, 4)

        # create group box
        self.tcp_server_group: QtWidgets.QGroupBox = QtWidgets.QGroupBox('TCP Server')
        self.tcp_server_group.setLayout(layout)
        return 
    
    def setup_device_manager(self) -> None:

        # create device manager
        self.device_manager: DeviceManager = DeviceManager(parent=self)
        self.device_manager_box: QtWidgets.QGroupbox = QtWidgets.QGroupBox('Devices')

        # create buttons
        default_device: str = CONF['general']['default_device']
        self.add_device_edit: QtWidgets.QLineEdit = QtWidgets.QLineEdit(text=default_device)
        self.add_device_edit.setPlaceholderText(default_device)
        self.add_device_button: QtWidgets.QPushButton = QtWidgets.QPushButton('Add') 

        # layout
        vlayout = QtWidgets.QVBoxLayout()
        hlayout = QtWidgets.QHBoxLayout()
        hlayout.addWidget(QtWidgets.QLabel('Device address:'))
        hlayout.addWidget(self.add_device_edit)
        hlayout.addWidget(self.add_device_button)
        vlayout.addLayout(hlayout)
        vlayout.addWidget(self.device_manager)
        self.device_manager_box.setLayout(vlayout)
    
    def connect_tcp_server(self) -> None:
        host = self.inp_host.text()
        port = int(self.inp_port.text())

        # Create the server
        msg = f"Starting TCP server on ({host}, {port})"
        logger.info(msg)
        try:
            self.server = tcp_server.TCPServer((host, port), tcp_server.TCPHandler)
        except:
            return

        # start server in separate thread
        threading.Thread(target=self.server.serve_forever, daemon=True).start()

        # update UI
        self.inp_host.setReadOnly(True)
        self.inp_port.setReadOnly(True)
        self.inp_connect.setEnabled(False)
    
    def poll_status(self) -> None:

        for addr, device in self.server.motor_threads.items():
            new_status = device.device_status
            self.device_manager.devices[addr].update_status(new_status=new_status)
            

    def add_device(self) -> None:

        device_address = self.add_device_edit.text()
        if not os.path.exists(device_address):
            logger.error(f"Device address {device_address} does not exist")
            return
        
        # try to connect motor
        if not self.server.connect_motor(device_address):
            logger.error(f"Could not connect to device at {device_address}")
            return

        # add device to device manager
        info = self.server.motor_threads[device_address].device_info
        self.device_manager.add_device(addr=device_address, model=info.model_no, serial=str(info.serial_no))


        return
    
def main():
    app = QtWidgets.QApplication(sys.argv)
    ex = ThorlabsMonitor()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
