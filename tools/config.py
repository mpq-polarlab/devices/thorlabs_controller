import os
import configparser

DEFAULT_CONFIGFILE: str = os.path.join(os.path.dirname(__file__), 'config.conf')
CONF: configparser.RawConfigParser = configparser.RawConfigParser()

local_configfile: str = DEFAULT_CONFIGFILE + '.local'

# read default config
with open(DEFAULT_CONFIGFILE) as f:
    CONF.read_file(f)

# read/write local config
if os.path.exists(local_configfile): # read local config and update default config
    with open(local_configfile) as f: 
        CONF.read_file(f)
else: # write default config to local config
    with open(local_configfile, 'w') as f:
        CONF.write(f)

