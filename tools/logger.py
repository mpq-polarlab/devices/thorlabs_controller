import logging
from colorlog import ColoredFormatter

# create formatter
FORMAT = "{log_color} {asctime} {levelname} {message} {reset} in {filename}:{lineno}"
formatter = ColoredFormatter(FORMAT, datefmt='%Y-%m-%d %H:%M:%S', style='{')

# create logger
logger = logging.getLogger("Thorlabs logger")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

if __name__ == "__main__":

    # 'application' code
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')

