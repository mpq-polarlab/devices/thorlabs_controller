import argparse
import tcp_server
import gui

parser = argparse.ArgumentParser()
parser.add_argument('--no-gui', action='store_true')
args = parser.parse_args()


def main(use_gui: bool = False):

    if use_gui == True:
        gui.main()
    else:
        tcp_server.main()

if __name__ == "__main__":
    main(use_gui=(not args.no_gui))

